# Semantic Looseleaf Workspace Tooling

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The 'Semantic Looseleaf Workspace' format and layout is a set of [version-control](https://en.wikipedia.org/wiki/Version_control) and [content-management](https://en.wikipedia.org/wiki/Content_management_system) conventions that lets you generate a portfolio for organizing your creative endeavors using [YAML](https://en.wikipedia.org/wiki/YAML) front matter.  This repository contains the format's tooling.  Its specification lives [here](https://gitlab.com/semantic-looseleaf-workspace/specification).  

## Prerequisites

 - A [text editor](https://en.wikipedia.org/wiki/List_of_text_editors).  
 - A relatively recent version of [Git](https://en.wikipedia.org/wiki/Git).  
 - Knowledge concerning:  
   - YAML (A quick-start primer may be found located [here](http://web.archive.org/web/20200114195518/https://learnxinyminutes.com/docs/yaml/).)  
 - Some free drive space.  

## Getting Started

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To begin:  

 1. Create a new Semantic Looseleaf Workspace and lay it out [as described in the format's specification](https://gitlab.com/semantic-looseleaf-workspace/specification/-/blob/main/README.md#anatomy-of-a-workspace).  
 1. Run this tooling[^1] over your Semantic Looseleaf Workspace to process it.  This will, for each file in 'All Content (Unprocessed:)'  
    - Copy it into a new or pre-existing top-level folder outside of 'All Content (Unprocessed.)'  
    - Merge copies of all keys, along with their values, from '`All Content (Unprocessed)/Shared Metadata.yaml`' into its front matter.  
    - Insert it wherever it belongs in the tag index.[^2]  
    - Fix any links embedded in it which point to other files in 'All Content (Unprocessed)' up to point to their processed counterparts.[^3]  
 1. Repeat step 2 as needed to make processed files reflect edits made to their unprocessed counterparts in 'All Content (Unprocessed)' and process any new files added there.  

You should now have a Semantic Looseleaf Workspace ready to use (and maybe even share and collaborate on!)  

## Supported Content File Formats

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;At the moment, the only content file format supported for use inside a Semantic Looseleaf Workspace is:  

 - Markdown

## Current Limitations

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TODO/FIXME:  

 - <a id="Current-Limitations-bullet-point-1"></a>Quoting !1, "The tooling needed to process a semantic looseleaf workspace into a fully presentable, production-ready form has yet to be written."  
 - File-format support currently remains quite limited.  See ['Supported Content File Formats'](#Supported-Content-File-Formats) for more details.  (Other formats may work if they support YAML front matter in some way, but their usability is by _no_ means guaranteed to be considered production-ready at this time.)  (Note also !5.)  
 - <a id="Current-Limitations-bullet-point-3"></a>Tag indexing has yet to be implemented.  No tag index page will get generated.  (Fixing this is tracked by !2.)  
 - <a id="Current-Limitations-bullet-point-4"></a>Similarly, link fix-up has yet to be implemented.  (Fixing this is tracked by !3.)  

## Inspirations/Background<!--Section title heading possibly subject to change/revision…?  -->

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The 'Semantic Looseleaf Workspace' format takes inspirational cues from:  

 - [GitHub user refractorsaurusrex](https://github.com/refactorsaurusrex)'s ['`journal-cli`' project](https://github.com/refactorsaurusrex/journal-cli).  
 - [Static web site generators](https://en.wikipedia.org/wiki/Web_template_system#Static_site_generators) like [Jekyll](https://en.wikipedia.org/wiki/Jekyll_(software)).  

Any further idiosyncrasies in this project derive from its author&thinsp;—&thinsp;that is, me.  

---

## Endnotes

[^1]:  See [the 'Current Limitations' section](#Current-Limitations), [bullet point 1](#Current-Limitations-bullet-point-1).  
[^2]:  See [the 'Current Limitations' section](#Current-Limitations), [bullet point 3](#Current-Limitations-bullet-point-3).  
[^3]:  See [the 'Current Limitations' section](#Current-Limitations), [bullet point 4](#Current-Limitations-bullet-point-4).  